package delivery

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/delivery/container"
	deliveryWs "gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/delivery/websocket"
)

func Run(c *container.Container) {
	deliveryWs.Run(c)
}
