// go:build wireinject
//go:build wireinject
// +build wireinject

package container

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/config"
	brokerclient "gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/helper/broker_client"
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/usecase/consumer"

	"github.com/google/wire"
)

type Container struct {
	AppConfig           config.AppConfig
	BrokerClientManager brokerclient.BrokerClientManager
	ConsumerUsecase     consumer.ConsumerUsecase
}

func SetupContainer(
	AppConfig config.AppConfig,
	BrokerClientManager brokerclient.BrokerClientManager,
	ConsumerUsecase consumer.ConsumerUsecase,
) *Container {
	return &Container{
		AppConfig:           AppConfig,
		BrokerClientManager: BrokerClientManager,
		ConsumerUsecase:     ConsumerUsecase,
	}
}

func InitializeContainer() *Container {
	wire.Build(
		SetupContainer,
		config.LoadConfigFromEnv,
		brokerclient.NewBrokerClientManager,
		consumer.NewConsumerUsecase,
	)
	return &Container{}
}
