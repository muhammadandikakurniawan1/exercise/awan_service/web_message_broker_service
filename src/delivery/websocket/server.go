package websocket

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/delivery/container"
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/delivery/websocket/handler"

	"github.com/gorilla/mux"
)

func Run(c *container.Container) {
	appChan := make(chan os.Signal, 1)
	signal.Notify(appChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)

	route := mux.NewRouter()

	consumerHandler := handler.NewWsConsumerHandler(c.ConsumerUsecase)

	route.HandleFunc("/subscribe", consumerHandler.Subscribe)
	route.HandleFunc("/publish", consumerHandler.Publish).Methods(http.MethodPost)

	port := c.AppConfig.WebSocketServerConfig.Port
	log.Println(fmt.Sprintf("WS running on port %d.", port))
	log.Println(http.ListenAndServe(fmt.Sprintf(":%d", port), route))

	<-appChan
	log.Println("WEBSOCKET Shutdown")
}
