package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/pkg/apperror"
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/usecase/consumer"

	gorillaWS "github.com/gorilla/websocket"
)

func NewWsConsumerHandler(consumerUsecase consumer.ConsumerUsecase) *WsConsumerHandler {
	return &WsConsumerHandler{
		consumerUsecase: consumerUsecase,
	}
}

type WsConsumerHandler struct {
	consumerUsecase consumer.ConsumerUsecase
}

func (h WsConsumerHandler) Subscribe(w http.ResponseWriter, r *http.Request) {
	topicName, consumerGroupName, consumerNode, err := h.consumerUsecase.RegisterConsumer(w, r)
	if err != nil {
		appErrr, isAppErr := err.(apperror.AppError)
		if isAppErr {
			http.Error(w, err.Error(), appErrr.GetErrorCode().ToHttpStatus())
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	go func() {
		for {
			_, messageByte, err := consumerNode.Consumer.ReadMessage()
			if err != nil {
				if gorillaWS.IsCloseError(err, gorillaWS.CloseNormalClosure, gorillaWS.CloseGoingAway) {
					h.consumerUsecase.DeleteConsumer(topicName, consumerGroupName, *consumerNode)
					return
				}
			}

			fmt.Println("=========================== new message to " + topicName + " ===============================")
			fmt.Println(messageByte)
			fmt.Println("===========================-============================================================")
		}
	}()
}

func (h WsConsumerHandler) Publish(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var reqBody consumer.SendPayloadRequest
	err := decoder.Decode(&reqBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.consumerUsecase.SendPayload(reqBody)
	if err != nil {
		appErrr, isAppErr := err.(apperror.AppError)
		if isAppErr {
			http.Error(w, err.Error(), appErrr.GetErrorCode().ToHttpStatus())
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]interface{}{
		"success": true,
	})
}
