package consumer

import (
	"net/http"
	"strings"

	brokerclient "gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/helper/broker_client"
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/pkg/apperror"
)

type ConsumerUsecase interface {
	SendPayload(request SendPayloadRequest) (err error)
	RegisterConsumer(w http.ResponseWriter, r *http.Request) (topicName, consumerGroupName string, c *brokerclient.LinkedListConsumerNode, e error)
	DeleteConsumer(topicName, consumerGroup string, consumerNode brokerclient.LinkedListConsumerNode) (err error)
}

func NewConsumerUsecase(
	brokerClientManager brokerclient.BrokerClientManager,
) ConsumerUsecase {
	return &consumerUsecaseImpl{
		brokerClientManager: brokerClientManager,
	}
}

type consumerUsecaseImpl struct {
	brokerClientManager brokerclient.BrokerClientManager
}

func (uc consumerUsecaseImpl) SendPayload(request SendPayloadRequest) (err error) {

	request.TopicName = strings.ReplaceAll(request.TopicName, " ", "")
	if request.TopicName == "" {
		err = apperror.NewAppError(apperror.ERROR_BAD_REQUEST, "topic name cannot be empty", "topic name cannot be empty")
		return
	}
	return uc.brokerClientManager.SendPayloadToConsumer(request.TopicName, []byte(request.PayloadString))
}

func (uc consumerUsecaseImpl) RegisterConsumer(w http.ResponseWriter, r *http.Request) (topicName, consumerGroupName string, newConsNode *brokerclient.LinkedListConsumerNode, err error) {

	topicName = strings.ReplaceAll(r.URL.Query().Get("topic"), " ", "")
	if topicName == "" {
		err = apperror.NewAppError(apperror.ERROR_BAD_REQUEST, "topic cannot be empty", "topic cannot be empty")
		return
	}

	consumerGroupName = strings.ReplaceAll(r.URL.Query().Get("consumer_group"), " ", "")
	newConn, err := uc.brokerClientManager.CreateConnection(w, r)
	if err != nil {
		return
	}
	newConsNode = uc.brokerClientManager.AddTopicConsumer(topicName, consumerGroupName, newConn)
	return
}

func (uc consumerUsecaseImpl) DeleteConsumer(topicName, consumerGroup string, consumerNode brokerclient.LinkedListConsumerNode) (err error) {
	deleteConsumerCommand := brokerclient.NewDeleteConsumerCommand(uc.brokerClientManager, topicName, consumerGroup, consumerNode)
	err = deleteConsumerCommand.Execute()
	return
}
