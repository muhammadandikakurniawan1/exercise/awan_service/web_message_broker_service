package consumer

type SendPayloadRequest struct {
	TopicName     string `json:"topic_name"`
	PayloadByte   []byte `json:"payload_byte"`
	PayloadString string `json:"payload_string"`
}
