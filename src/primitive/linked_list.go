package primitive

type DobleLinkedList[T any] interface {
	GetNext() T
	GetPrevious() T
	GetHead() T
	AddNext(consumer T)
}
