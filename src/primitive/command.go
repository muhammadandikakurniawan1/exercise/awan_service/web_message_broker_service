package primitive

type Command interface {
	Execute() (err error)
	Undo() (err error)
}
