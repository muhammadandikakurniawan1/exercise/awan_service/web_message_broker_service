package primitive

import (
	"github.com/gorilla/websocket"
)

type WsConnectionSendComponent interface {
	SendPayload(payload []byte) (err error)
}

type WsConnection interface {
	WsConnectionSendComponent
	GetId() string
	GetConn() *websocket.Conn
	ReadMessage() (messageType int, p []byte, err error)
}

type WsConnectionGroup interface {
	WsConnectionSendComponent
	Add(conn WsConnection) WsConnection
	DeleteById(id string) WsConnection
}
