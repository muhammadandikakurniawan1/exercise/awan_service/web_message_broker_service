package config

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/gopkg/circuitbreaker"
)

type AppConfig struct {
	HttpClientConfig      HttpClientConfig
	GoogleServiceConfig   GoogleServiceConfig
	HttpServerConfig      ServerConfig
	WebSocketServerConfig ServerConfig
	DbConfig              DbConfig
	CircuitbreakerConfig  circuitbreaker.Config
}
