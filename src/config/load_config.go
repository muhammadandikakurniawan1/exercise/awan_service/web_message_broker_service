package config

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/gopkg/utility"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

func LoadConfigFromEnv() AppConfig {

	if err := godotenv.Load(); err != nil {
		// panic(err)
	}
	return AppConfig{
		// HttpServerConfig: ServerConfig{
		// 	Host: utility.GetEnvPanic("HTTP_HOST"),
		// 	Port: cast.ToInt(utility.GetEnvPanic("HTTP_PORT")),
		// },
		WebSocketServerConfig: ServerConfig{
			Host: utility.GetEnvPanic("WEBSOCKET_HOST"),
			Port: cast.ToInt(utility.GetEnvPanic("WEBSOCKET_PORT")),
		},
		// DbConfig: DbConfig{
		// 	Host:     utility.GetEnvPanic("DB_POSTGRESQL_HOST"),
		// 	Port:     cast.ToInt(utility.GetEnvPanic("DB_POSTGRESQL_PORT")),
		// 	User:     utility.GetEnvPanic("DB_POSTGRESQL_USER"),
		// 	Password: utility.GetEnvPanic("DB_POSTGRESQL_PASSWORD"),
		// 	Db:       utility.GetEnvPanic("DB_POSTGRESQL_DB"),
		// },
		// GoogleServiceConfig: GoogleServiceConfig{
		// 	GetUserInfoEndpoint: utility.GetEnvPanic("GOOGLE_SERVICE_GET_USERINFO_ENDPOINT"),
		// },
		// HttpClientConfig: HttpClientConfig{
		// 	MaxRetry:          cast.ToInt(utility.GetEnv("HTTP_CLIENT_MAX_RETRY", "1")),
		// 	TimeoutNanoSecond: time.Duration(cast.ToInt(utility.GetEnv("HTTP_CLIENT_TIMEOUT", "2000000000"))), // default 2s
		// },
		// CircuitbreakerConfig: circuitbreaker.Config{
		// 	Name:        utility.GetEnv("CIRCUIT_BREAKER_CONFIG_NAME", "cb_setting"),
		// 	MaxRequests: cast.ToUint32(utility.GetEnv("CIRCUIT_BREAKER_MAX_REQUEST", "3")),
		// 	Timeout:     cast.ToDuration(utility.GetEnv("CIRCUIT_BREAKER_TIMEOUT", "5000000000")),  // default 5s
		// 	Interval:    cast.ToDuration(utility.GetEnv("CIRCUIT_BREAKER_INTERVAL", "5000000000")), // default 5s
		// },
	}
}
