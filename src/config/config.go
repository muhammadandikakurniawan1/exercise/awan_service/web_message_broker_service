package config

import "time"

type DbConfig struct {
	Host     string
	Port     int
	Db       string
	User     string
	Password string
}

type ServerConfig struct {
	Host string
	Port int
}

type GoogleServiceConfig struct {
	GetUserInfoEndpoint string
}

type HttpClientConfig struct {
	MaxRetry          int
	TimeoutNanoSecond time.Duration
}
