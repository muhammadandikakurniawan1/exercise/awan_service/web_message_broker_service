package entity

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/gopkg/primitive"
)

type MapConsumerById map[string]Consumer

type Consumer struct {
	primitive.BaseEntity
	Id            string
	ConsumerGroup ConsumerGroup
}

func (c Consumer) Send(payload []byte) (err error) {
	// send to connection
	return
}
