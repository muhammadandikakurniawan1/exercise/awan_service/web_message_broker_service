package entity

type MapConsumerGroupByName map[string]ConsumerGroup

type ConsumerGroup struct {
	Name      string
	Consumers MapConsumerById
}

func (c ConsumerGroup) Send(payload []byte) (err error) {
	// send to connections
	for _, consumer := range c.Consumers {
		consumer.Send(payload)
	}
	return
}
