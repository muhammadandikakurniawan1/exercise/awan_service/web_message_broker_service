package entity

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/gopkg/primitive"
)

type Topic struct {
	primitive.BaseEntity
	TopicName      string
	Payload        []byte
	Consumers      MapConsumerById
	ConsumerGroups MapConsumerGroupByName
}
