package brokerclient

import (
	"net/http"

	pkgWebsocket "gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/pkg/websocket"
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/primitive"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

type BrokerClientManager interface {
	SendPayloadToConsumer(topic string, payload []byte) (err error)
	AddTopic(topicName string) *Topic
	AddTopicConsumer(topicName, consGroup string, conn primitive.WsConnection) *LinkedListConsumerNode
	GetTopicByName(name string) *Topic
	DeleteTopic(name string) *Topic
	CreateConnection(w http.ResponseWriter, r *http.Request) (primitive.WsConnection, error)
}

func NewBrokerClientManager() BrokerClientManager {
	wsUpgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	return &brokerClientManagerImpl{
		mapTopic:   map[string]*Topic{},
		wsUpgrader: wsUpgrader,
	}
}

type brokerClientManagerImpl struct {
	mapTopic   map[string]*Topic
	wsUpgrader websocket.Upgrader
}

func (b *brokerClientManagerImpl) CreateConnection(w http.ResponseWriter, r *http.Request) (primitive.WsConnection, error) {
	conn, err := b.wsUpgrader.Upgrade(w, r, w.Header())
	if err != nil {
		return nil, err
	}
	newWsConn := pkgWebsocket.WsConnection{
		Id:   uuid.NewString(),
		Conn: conn,
	}
	return &newWsConn, nil
}

func (b *brokerClientManagerImpl) GetTopicByName(name string) *Topic {
	if b == nil {
		return nil
	}
	return b.mapTopic[name]
}

func (b *brokerClientManagerImpl) DeleteTopic(name string) *Topic {
	if b == nil {
		return nil
	}

	topic, exists := b.mapTopic[name]
	if !exists {
		return topic
	}
	delete(b.mapTopic, name)
	return topic
}

func (b *brokerClientManagerImpl) SendPayloadToConsumer(topicName string, payload []byte) (err error) {
	topic, exists := b.mapTopic[topicName]
	if !exists {
		return
	}

	err = topic.SendPayload(payload)
	return
}

func (b *brokerClientManagerImpl) AddTopic(topicName string) *Topic {
	topic := NewTopic(topicName)

	b.mapTopic[topicName] = topic

	return topic
}

func (b *brokerClientManagerImpl) AddTopicConsumer(topicName, consGroup string, conn primitive.WsConnection) *LinkedListConsumerNode {

	topic, exists := b.mapTopic[topicName]
	if !exists {
		topic = b.AddTopic(topicName)
	}
	return topic.AddConsumer(consGroup, conn)
}
