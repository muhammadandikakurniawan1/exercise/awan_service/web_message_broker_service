package brokerclient

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/primitive"
)

func NewConsumerSendPayloadCommand(consumerState *ConsumerState, payload []byte, isHaveConsumerGroup bool) primitive.Command {
	return &ConsumerSendPayloadCommand{
		payload:             payload,
		consumerState:       consumerState,
		isHaveConsumerGroup: isHaveConsumerGroup,
	}
}

type ConsumerSendPayloadCommand struct {
	payload             []byte
	consumerState       *ConsumerState
	isHaveConsumerGroup bool
}

func (cmd *ConsumerSendPayloadCommand) Execute() (err error) {
	if cmd == nil {
		return
	}

	if cmd.consumerState == nil {
		return
	}

	if cmd.consumerState.CurrentConsumer == nil {
		return
	}

	if !cmd.isHaveConsumerGroup {
		tempState := cmd.consumerState.CurrentConsumer
		for tempState != nil {
			err = tempState.Consumer.Send(cmd.payload)
			if err != nil {
				return
			}
			tempState = tempState.next
		}
		return
	}

	err = cmd.consumerState.CurrentConsumer.Consumer.Send(cmd.payload)
	if err != nil {
		return
	}

	if next := cmd.consumerState.CurrentConsumer.GetNext(); next != nil {
		cmd.consumerState.CurrentConsumer = next
		return
	}

	if head := cmd.consumerState.Head; head != nil {
		cmd.consumerState.CurrentConsumer = head
	}

	return
}

func (cmd *ConsumerSendPayloadCommand) Undo() error {
	return nil
}
