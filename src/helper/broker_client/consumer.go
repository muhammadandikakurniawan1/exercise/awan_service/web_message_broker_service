package brokerclient

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/primitive"
)

type Consumer struct {
	connection primitive.WsConnection
}

func (c Consumer) Send(payload []byte) (err error) {
	return c.connection.SendPayload(payload)
}

func (c Consumer) ReadMessage() (messageType int, p []byte, err error) {
	return c.connection.ReadMessage()
}

func NewConsumerState(node *LinkedListConsumerNode) *ConsumerState {
	return &ConsumerState{
		CurrentConsumer: node,
		Head:            node,
		Tail:            node,
	}
}

type ConsumerState struct {
	Tail            *LinkedListConsumerNode
	Head            *LinkedListConsumerNode
	CurrentConsumer *LinkedListConsumerNode
}

func NewConsumer(wsConn primitive.WsConnection) *Consumer {
	return &Consumer{
		connection: wsConn,
	}
}

func (c *ConsumerState) AddConsumer(newConn primitive.WsConnection) *LinkedListConsumerNode {
	if c == nil {
		return nil
	}

	node := NewLinkedListConsumerNode(NewConsumer(newConn))
	node.state = c
	if c.Tail != nil {
		c.Tail.AddNext(node)
		c.Tail = node
		return node
	}

	if c.CurrentConsumer == nil {
		c.CurrentConsumer = node
		c.Head = node
		c.Tail = node
	}

	return node
}

func (l *ConsumerState) Delete(node *LinkedListConsumerNode) {
	l.setHeadDelete(node)
	l.setTailDelete(node)
	l.setCurrentDelete(node)
}

func (l *ConsumerState) setHeadDelete(node *LinkedListConsumerNode) {
	if l.Head == nil {
		return
	}
	isSameNode := l.Head.Consumer.connection.GetId() == node.Consumer.connection.GetId()
	if isSameNode {
		l.Head = l.Head.next
	}
}

func (l *ConsumerState) setTailDelete(node *LinkedListConsumerNode) {
	if l.Tail == nil {
		return
	}
	isSameNode := l.Tail.Consumer.connection.GetId() == node.Consumer.connection.GetId()
	if isSameNode {
		l.Tail = l.Tail.prev
	}
}

func (l *ConsumerState) setCurrentDelete(node *LinkedListConsumerNode) {
	if l.CurrentConsumer == nil {
		return
	}
	isSameNode := l.CurrentConsumer.Consumer.connection.GetId() == node.Consumer.connection.GetId()
	if isSameNode {
		if currentNext := l.CurrentConsumer.next; currentNext != nil {
			l.CurrentConsumer = currentNext
			return
		}
		l.CurrentConsumer = l.Head
	}
}
