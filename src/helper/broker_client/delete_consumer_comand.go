package brokerclient

import "gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/primitive"

func NewDeleteConsumerCommand(
	clientManager BrokerClientManager,
	topicName, consumerGroupName string,
	consumerNode LinkedListConsumerNode,
) primitive.Command {
	return &DeleteConsumerCommand{
		clientManager:     clientManager,
		topicName:         topicName,
		consumerGroupName: consumerGroupName,
		consumerNode:      consumerNode,
	}
}

type DeleteConsumerCommand struct {
	clientManager                BrokerClientManager
	topicName, consumerGroupName string
	consumerNode                 LinkedListConsumerNode
}

func (c DeleteConsumerCommand) Execute() (err error) {

	c.consumerNode.Delete()

	topic := c.clientManager.GetTopicByName(c.topicName)
	if topic == nil {
		return
	}

	consumerGroup, exists := topic.ConsumerGroup[c.consumerGroupName]
	if !exists {
		return
	}
	if consumerGroup == nil {
		delete(topic.ConsumerGroup, c.consumerGroupName)
	} else {
		if consumerGroup.Head == nil {
			delete(topic.ConsumerGroup, c.consumerGroupName)
		}
	}

	consumerGroupIsEmpty := len(topic.ConsumerGroup) <= 0
	if consumerGroupIsEmpty {
		c.clientManager.DeleteTopic(c.topicName)
		return
	}

	return
}
func (c DeleteConsumerCommand) Undo() (err error) {
	return
}
