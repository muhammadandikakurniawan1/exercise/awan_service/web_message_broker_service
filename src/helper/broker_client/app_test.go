package brokerclient

import (
	"fmt"
	"testing"

	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/pkg/websocket"
)

func Test_manager(t *testing.T) {
	manager := NewBrokerClientManager()

	topics := []string{"topic-1", "topic-2", "topic-3"}
	ids := []string{"userid-1", "userid-2", "userid-3", "userid-4", "userid-5", "userid-6", "userid-7", "userid-8", "userid-9"}

	manager.AddTopicConsumer(topics[0], "", websocket.WsConnection{
		Id: ids[0],
	})
	manager.AddTopicConsumer(topics[0], "cg1-1", websocket.WsConnection{
		Id: ids[1],
	})
	manager.AddTopicConsumer(topics[0], "cg1-2", websocket.WsConnection{
		Id: ids[2],
	})
	// ==========================================================================================
	manager.AddTopicConsumer(topics[1], "", websocket.WsConnection{
		Id: ids[3],
	})
	manager.AddTopicConsumer(topics[1], "", websocket.WsConnection{
		Id: ids[4],
	})
	manager.AddTopicConsumer(topics[1], "cg2-1", websocket.WsConnection{
		Id: ids[5],
	})
	manager.AddTopicConsumer(topics[1], "cg2-2", websocket.WsConnection{
		Id: ids[6],
	})
	manager.AddTopicConsumer(topics[1], "cg2-2", websocket.WsConnection{
		Id: ids[7],
	})
	manager.AddTopicConsumer(topics[1], "cg2-2", websocket.WsConnection{
		Id: ids[8],
	})

	for i := 1; i <= 5; i++ {
		msg := fmt.Sprintf("notif %d", i)
		manager.SendPayloadToConsumer(topics[1], []byte(msg))
		fmt.Println("==============================================================")
	}

	fmt.Println("finish")
}

func Test_manager_2(t *testing.T) {
	state := NewConsumerState(nil)
	cmd := NewConsumerSendPayloadCommand(state, []byte("message"), true)
	c1 := state.AddConsumer(websocket.WsConnection{Id: "con-1"})
	c2 := state.AddConsumer(websocket.WsConnection{Id: "con-2"})
	c3 := state.AddConsumer(websocket.WsConnection{Id: "con-3"})
	c4 := state.AddConsumer(websocket.WsConnection{Id: "con-4"})

	cmd.Execute()
	c2.Delete()
	cmd.Execute()
	c1.Delete()
	cmd.Execute()
	c4.Delete()
	cmd.Execute()
	cmd.Execute()
	cmd.Execute()
	fmt.Println(c1, c2, c3, c4)
}
