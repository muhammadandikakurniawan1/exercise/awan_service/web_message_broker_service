package brokerclient

func NewLinkedListConsumerNode(c *Consumer) *LinkedListConsumerNode {
	return &LinkedListConsumerNode{
		Consumer: c,
	}
}

type LinkedListConsumerNode struct {
	Consumer *Consumer
	state    *ConsumerState
	next     *LinkedListConsumerNode
	prev     *LinkedListConsumerNode
}

func (l *LinkedListConsumerNode) GetNext() *LinkedListConsumerNode {
	return l.next
}

func (l *LinkedListConsumerNode) GetPrevious() *LinkedListConsumerNode {
	return l.prev
}

func (l *LinkedListConsumerNode) AddNext(consumer *LinkedListConsumerNode) {
	if l == nil {
		return
	}

	consumer.prev = l
	l.next = consumer
}

func (l *LinkedListConsumerNode) Delete() {
	if l == nil {
		return
	}
	if l.state == nil {
		return
	}

	l.state.Delete(l)

	next := l.next
	prev := l.prev
	if next != nil {
		next.prev = prev
	}
	if prev != nil {
		prev.next = next
	}
}
