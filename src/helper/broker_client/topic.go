package brokerclient

import (
	"strings"
	"sync"

	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/primitive"
)

func NewTopic(topicName string) *Topic {
	return &Topic{
		Name:          topicName,
		ConsumerGroup: map[string]*ConsumerState{},
	}
}

type Topic struct {
	Name          string
	ConsumerGroup map[string]*ConsumerState
}

func (t *Topic) AddConsumer(group string, conn primitive.WsConnection) *LinkedListConsumerNode {
	group = strings.ReplaceAll(group, " ", "")
	consState, exists := t.ConsumerGroup[group]
	if !exists {
		consState = NewConsumerState(nil)
		t.ConsumerGroup[group] = consState
	}

	return consState.AddConsumer(conn)
}

func (t *Topic) SendPayload(payload []byte) (err error) {

	wg := sync.WaitGroup{}
	errors := make([]error, 2)
	wg.Add(2)

	go func() {
		defer wg.Done()
		consumerStateWithoutGroup, exists := t.ConsumerGroup[""]
		if exists {
			cmd := NewConsumerSendPayloadCommand(consumerStateWithoutGroup, payload, false)
			errors[0] = cmd.Execute()
			if errors[0] != nil {
				return
			}
		}
	}()

	go func() {
		defer wg.Done()
		for consumerGroupName, consumerState := range t.ConsumerGroup {
			isEmptyGroup := consumerGroupName == ""
			if isEmptyGroup {
				continue
			}
			cmd := NewConsumerSendPayloadCommand(consumerState, payload, true)
			errors[1] = cmd.Execute()
			if errors[1] != nil {
				return
			}
		}
	}()

	wg.Wait()

	if err = errors[0]; err != nil {
		return
	}
	if err = errors[1]; err != nil {
		return
	}

	return
}
