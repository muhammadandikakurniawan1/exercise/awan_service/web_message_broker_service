package composite

type ConsumerComposite interface {
	Send(payload []byte) (err error)
}
