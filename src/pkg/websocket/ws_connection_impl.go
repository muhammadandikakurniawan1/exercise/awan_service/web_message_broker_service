package websocket

import (
	"github.com/gorilla/websocket"
)

type WsConnection struct {
	Id   string
	Conn *websocket.Conn
}

func (c WsConnection) SendPayload(payload []byte) (err error) {
	return c.Conn.WriteMessage(1, payload)
	// fmt.Printf(`{
	// 	"receiver" : %s,
	// 	"payload" : %s
	// }
	// `, c.Id, string(payload))
	// return
}

func (c WsConnection) GetId() string {
	return c.Id
}
func (c WsConnection) GetConn() *websocket.Conn {
	return c.Conn
}

func (c WsConnection) ReadMessage() (messageType int, p []byte, err error) {
	return c.Conn.ReadMessage()
}
