package websocket

import (
	"fmt"
	"testing"

	"github.com/gorilla/websocket"
)

func Test_map_client(t *testing.T) {
	mc := NewMapWsConnection()

	// ids := []string{
	// 	uuid.NewString(),
	// 	uuid.NewString(),
	// 	uuid.NewString(),
	// 	uuid.NewString(),
	// 	uuid.NewString(),
	// }

	ids := []string{"1", "2", "3", "4", "5"}

	mc.SendPayload([]byte("payload1"))
	mc.Add(WsConnection{
		Id:   ids[0],
		Conn: &websocket.Conn{},
	})

	mc.SendPayload([]byte("payload2"))
	mc.Add(WsConnection{
		Id:   ids[1],
		Conn: &websocket.Conn{},
	})

	mc.SendPayload([]byte("payload3"))
	mc.Add(WsConnection{
		Id:   ids[2],
		Conn: &websocket.Conn{},
	})

	mc.SendPayload([]byte("payload4"))
	mc.Add(WsConnection{
		Id:   ids[3],
		Conn: &websocket.Conn{},
	})

	mc.SendPayload([]byte("payload5"))
	mc.Add(WsConnection{
		Id:   ids[4],
		Conn: &websocket.Conn{},
	})

	mc.SendPayload([]byte("payload all"))

	mc.DeleteById(ids[2])
	mc.SendPayload([]byte("payload 5"))

	mc.DeleteById(ids[1])
	mc.SendPayload([]byte("payload 6"))

	mc.DeleteById(ids[3])
	mc.SendPayload([]byte("payload 7"))

	fmt.Println("================== finish =================")
}
