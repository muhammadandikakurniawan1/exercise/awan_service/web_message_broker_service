package websocket

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/primitive"
)

func NewMapWsConnection() primitive.WsConnectionGroup {
	var res MapWsConnection
	res = map[string]primitive.WsConnection{}
	return &res
}

type MapWsConnection map[string]primitive.WsConnection

func (c *MapWsConnection) SendPayload(payload []byte) (err error) {
	if c == nil {
		return
	}
	for _, conn := range *c {
		if err = conn.SendPayload(payload); err != nil {
			return
		}
	}
	return
}

func (c *MapWsConnection) DeleteById(id string) primitive.WsConnection {
	if c == nil {
		return nil
	}

	conn, exists := (*c)[id]
	if !exists {
		return nil
	}
	delete(*c, id)
	return conn
}

func (c *MapWsConnection) Add(conn primitive.WsConnection) primitive.WsConnection {
	(*c)[conn.GetId()] = conn
	return conn
}
