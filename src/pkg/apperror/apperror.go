package apperror

import (
	"fmt"
)

func GetErrorType(errCode int) HttpErrorType {
	switch {
	case errCode >= 200 && errCode <= 299:
		return HTTP_SUCCESS
	case errCode >= 400 && errCode <= 499:
		return HTTP_INVALID_REQUEST
	case errCode >= 500 && errCode <= 599:
		return HTTP_INTERNAL_SERVER_ERROR
	default:
		return -1

	}
}

func NewAppError(errCode ErrorCode, msg, errMsg string) error {
	return &AppError{
		ErrorCode:          errCode,
		Message:            msg,
		SystemErrorMessage: errMsg,
	}
}

type AppError struct {
	ErrorCode          ErrorCode `json:"error_code"`
	Message            string    `json:"message"`
	SystemErrorMessage string    `json:"system_error_message"`
}

func (er AppError) Error() string {
	return fmt.Sprintf("[%s] | %s | %s", er.ErrorCode, er.Message, er.SystemErrorMessage)
}

func (er AppError) GetErrorCode() ErrorCode {
	return er.ErrorCode
}

func (er AppError) GetMessage() string {
	return er.Message
}

func (er AppError) GetSystemErrorMessage() string {
	return er.SystemErrorMessage
}
