package apperror

import "net/http"

type ErrorCode string
type HttpErrorType int

func (e ErrorCode) ToHttpStatus() int {
	switch e {
	case ERROR_UNAUTHORIZED:
		return http.StatusUnauthorized
	case ERROR_SURROUNDING:
		return http.StatusInternalServerError
	case ERROR_BAD_REQUEST:
		return http.StatusBadRequest
	}

	return http.StatusInternalServerError
}

const (
	ERROR_UNAUTHORIZED ErrorCode = ErrorCode("AUTH401")
	ERROR_SURROUNDING  ErrorCode = ErrorCode("AUTH501")
	ERROR_BAD_REQUEST  ErrorCode = ErrorCode("AUTH400")
)

const (
	HTTP_SUCCESS HttpErrorType = iota
	HTTP_INTERNAL_SERVER_ERROR
	HTTP_INVALID_REQUEST
)
