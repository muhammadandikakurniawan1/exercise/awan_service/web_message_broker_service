package main

import (
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/delivery"
	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service/src/delivery/container"
)

func main() {
	dependencyContainer := container.InitializeContainer()

	delivery.Run(dependencyContainer)
}
