PACKAGE_NAME := user_service

container-run:
	docker run  -e WEBSOCKET_PORT=2412 -e WEBSOCKET_HOST='localhost' -p 2412:2412 -i andika6690/awan_service-web_message_broker_service 

deploy:
	docker-compose up --build -d

wire:
	go install github.com/google/wire/cmd/wire@latest
	go get github.com/google/wire/cmd/wire
	go run github.com/google/wire/cmd/wire

run:
	cd cmd/app 
	go run main.go

install:
	go get golang.org/x/tools/cmd/cover
	go install github.com/google/wire/cmd/wire@latest
	go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
	go mod download
	go mod tidy

gen-proto:
	protoc --go_out=. --go-grpc_out=. src/protobuf/userservice/user/*.proto
	protoc --go_out=. --go-grpc_out=. src/protobuf/trackingservice/tracking/*.proto

test:
	@echo "=================================================================================="
	@echo "Coverage Test"
	@echo "=================================================================================="
	go fmt ./... && go test -coverprofile coverage.cov -cover ./... # use -v for verbose
	@echo "\n"
	@echo "=================================================================================="
	@echo "All Package Coverage"
	@echo "=================================================================================="
	go tool cover -func coverage.cov

mock:
	@echo "=================================================================================="
	@echo "Generating Mock"
	@echo "=================================================================================="
	@echo "Make sure install mockery https://github.com/vektra/mockery#installation"
	@echo "\n"
	mockery --all --output internal/mocks --case underscore