module gitlab.com/muhammadandikakurniawan1/exercise/awan_service/web_message_broker_service

go 1.18

require (
	github.com/joho/godotenv v1.5.1
	github.com/spf13/cast v1.5.0
)

require gitlab.com/muhammadandikakurniawan1/exercise/awan_service/gopkg v0.0.0-20230225010807-111908e7fb12

require (
	github.com/google/uuid v1.3.0
	github.com/google/wire v0.5.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0
	github.com/sony/gobreaker v0.5.0 // indirect
)
